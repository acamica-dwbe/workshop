class Usuario {

  constructor(nombre, apellido, pais, email, clave) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.pais = pais;
    this.email = email;
    this.clave = clave;
  }

  compararEmailClave(clave, email) {
    return (this.clave === clave && this.email === email);
  }

}